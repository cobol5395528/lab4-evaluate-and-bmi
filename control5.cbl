       IDENTIFICATION DIVISION.
       PROGRAM-ID. CONTROL5.
       AUTHOR. PHADOL WONGSIRI.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  VALIDATION-RETURN-CODE  PIC   9 VALUE 9.
           88 DATE-IS-OK           VALUE 0.
           88 DATE-IS-INVALID      VALUE 1 THRU 8.
           88 VALID-CODE-SUPPLIED  VALUE 0 THRU 8.
       01  DATE-ERROR-MESSAGE      PIC X(35) VALUE SPACES.
           88 DATE-NOT-NUMERIC     VALUE "Error - the date must be numer
      -    "ic.".
           88 YEAR-IS-ZERO         VALUE "Error - The year cannot be zer
      -    "o.".
           88 MONTH-IS-ZERO        VALUE "Error - The month cannot be ze
      -    "ro.".
           88 DAY-IS-ZERO          VALUE "Error - The day cannot be zero
      -    ".".
           88 YEAR-PASSED          VALUE "Error - Year has already passe
      -    "d.".
           88 MONTH-TOO-BIG        VALUE "Error - Month is greater than
      -    "12.".
           88 DAY-TOO-BIG          VALUE "Error - Day is greater than 31
      -    ".".
           88 TOO-BIG-FOR-MONTH    VALUE "Error - Day too big for this m
      -    "onth.".

       PROCEDURE DIVISION .
       BEGIN.
           PERFORM VALIDATE-DATE UNTIL VALID-CODE-SUPPLIED
           EVALUATE VALIDATION-RETURN-CODE
              WHEN 0 SET DATE-IS-OK TO TRUE
              WHEN 1 SET DATE-NOT-NUMERIC TO TRUE
              WHEN 2 SET YEAR-IS-ZERO TO TRUE
              WHEN 3 SET MONTH-IS-ZERO TO TRUE
              WHEN 4 SET DAY-IS-ZERO TO TRUE
              WHEN 5 SET YEAR-PASSED TO TRUE
              WHEN 6 SET MONTH-TOO-BIG TO TRUE
              WHEN 7 SET DAY-TOO-BIG TO TRUE
              WHEN 8 SET TOO-BIG-FOR-MONTH TO TRUE
           END-EVALUATE
           .

           IF DATE-IS-INVALID THEN
              DISPLAY DATE-ERROR-MESSAGE
           END-IF 

           IF DATE-IS-OK THEN
              DISPLAY "Date is OK"
           END-IF
           GOBACK
           .

       VALIDATE-DATE.
           DISPLAY  "Enter a validation return code (0-8) - " WITH NO AD
      -    VANCING
           ACCEPT VALIDATION-RETURN-CODE
           .