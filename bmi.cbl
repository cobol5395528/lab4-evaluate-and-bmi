       IDENTIFICATION DIVISION.
       PROGRAM-ID. BMICAL.
       AUTHOR. PHADOL WONGSIRI.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 WS-WEIGHT  PIC 9(3)V99.  
       01 WS-HEIGHT  PIC 9(3)V99.  
       01 WS-M-HEIGHT PIC 9V99.
       01 WS-BMI      PIC 9(3)V99. 
       01 WS-MASK    PIC ZZ9.99.
       01 M-TEXT       PIC X(99).

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Enter your weight (kg): "
              WITH NO ADVANCING
           ACCEPT WS-WEIGHT 
           DISPLAY "Enter your height (cm): "
              WITH NO ADVANCING
           ACCEPT WS-HEIGHT 
           MOVE WS-WEIGHT TO WS-MASK
           MOVE WS-HEIGHT TO WS-MASK
           COMPUTE WS-M-HEIGHT = WS-HEIGHT/100
           COMPUTE WS-BMI = (WS-WEIGHT / (WS-M-HEIGHT * WS-M-HEIGHT))
           MOVE WS-BMI TO WS-MASK
           DISPLAY 'BMI: ' WS-MASK 
           EVALUATE TRUE
              WHEN WS-BMI > 30    
                 MOVE "You are obese class III"   TO M-TEXT
              WHEN WS-BMI > 25    
                 MOVE "You are obese class II"    TO M-TEXT
              WHEN WS-BMI > 23    
                 MOVE "You are obese class I"     TO M-TEXT
              WHEN WS-BMI >= 18.5  
                 MOVE "You are a normal person."  TO M-TEXT
              WHEN WS-BMI < 18.5  
                 MOVE "You are a very thin person."  TO M-TEXT
           END-EVALUATE
           DISPLAY M-TEXT
           GOBACK
           .